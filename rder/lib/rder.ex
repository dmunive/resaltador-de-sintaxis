defmodule Rder do

  def lexerJson do
    File.read!("test.json")
  |> String.to_charlist
  end



  def asignar(:comma,value), do: "<span style=\"color:red\" >" <> (value |> to_string) <> "</span>"
  def asignar(:inicio_llave,value), do: "<span style=\"color:Magenta\" >" <> (value |> to_string) <> "</span>"
  def asignar(:fin_llave,value), do: "<span style=\"color:Magenta\" >" <> (value |> to_string) <> "</span>"
  def asignar(:inicio_corchete,value), do: "<span style=\"color:Orange\" >" <> (value |> to_string) <> "</span>"
  def asignar(:fin_corchete,value), do: "<span style=\"color:Orange\" >" <> (value |> to_string) <> "</span>"
  def asignar(:dolbe_punto,value), do: "<span style=\"color:red\" >" <> (value |> to_string) <> "</span>"
  def asignar(:int,value), do: "<span style=\"color:blue\" >" <> (value |> to_string) <> "</span>"
  def asignar(:float,value), do: "<span style=\"color:blue\" >" <> (value |> to_string) <> "</span>"
  def asignar(:string,value), do: "<span style=\"font-weight:bold\" > \"" <> (value |> to_string) <> "\" </span>"
  def asignar(:bool,value), do: "<span style=\"color:DarkOrchid\" >" <> (value |> to_string) <> "</span>"
  def asignar(:null,value), do: "<span style=\"color:DarkOrchid\" >" <> (value |> to_string) <> "</span>"
  def asignar(:enters,_), do: "<br>"

  def tipo do

    code = :lexer.string(lexerJson())
    |> elem(1)
    |> Enum.map(fn {token,_,value} -> asignar(token, value) end)
    |> Enum.join
     |> skeleton()

    {:ok, file} = File.open("index.html", [:write])
     IO.write(file, code)


  end



  def skeleton(codigo) do
    """
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
    </head>
    <body>
    <pre>
    <code>
    #{codigo}
    </code>
    </pre>
    </body>
    </html>
    """

      end

end
